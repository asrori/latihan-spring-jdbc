/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asrori.springbootnew.dao;

import com.asrori.springbootnew.model.Karyawan;
import java.util.List;

/**
 *
 * @author USER
 */
public interface KaryawanDao {
    List<Karyawan> findAll();
    Karyawan findById(Integer id);
    void deleteById(Integer id);
    void add(Karyawan karyawan);
    void update(Karyawan karyawan);
}
