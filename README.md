CREATE DATABASE
====================================================

CREATE DATABASE  IF NOT EXISTS `belajar_spring_boot`;
USE `belajar_spring_boot`;
DROP TABLE IF EXISTS `karyawan`;


CREATE TABLE
====================================================
CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `gaji` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


INSERT DATA
====================================================
INSERT INTO `karyawan` VALUES (1,'Budi Luhur','1995-12-19',2000000.00),(2,'Sri Rejeki','1993-03-22',1500000.00);
